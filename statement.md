My PhD research is focussed on the calculation of amplitudes at the precision frontier of QCD.
This presents the challenge of handling the algebraic and analytic complexity of higher order expressions, and carefully regulating the infrared behaviour to cancel poles at each perturbative order.
I work with other members of Simon Badger's JetDynamics group to implement analytical expressions of these amplitudes into the library `NJet`[@Badger:2012pg].
The results provide a vital ingredient for theoretical predicitions of cross-sections at the LHC.

With an ongoing view to the six-point process ($gg\to\gamma\gamma gg$), we recently completed the loop-induced gluon fusion to diphoton plus jet ($gg\to\gamma\gamma g$).
We first calculated the amplitude by summing over permutations of the existing five-gluon result as in Ref.[@deFlorian:1999tp].
We then obtained efficient analytic expressions, in particular using reconstruction over finite fields[@Peraro:2019svx], and validated against the previous result.
We are also studying the feasibility of using neural networks to optimise the cross-section calculation of this process, following related work for leptonic colliders[@Badger:2020uow].

I am extending `NJet` to include a library of the various soft and collinear functions of partonic amplitudes.
These limits will provide the building blocks of counterterms to regulate infrared divergences within subtraction schemes at NNLO[@Herzog:2018ily].

Following the availablilty of an analytic library of the two-loop integrals[@Chicherin:2020oor], we have been working on implementing the coefficients within `NJet` to obtain the five-gluon two-loop amplitude at leading colour.
