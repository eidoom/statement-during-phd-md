DOCNAME=statement

all: $(DOCNAME).pdf

.PHONY: view clean

tmp.bib: $(DOCNAME).bib linkify.sh
	./linkify.sh $< $@

$(DOCNAME).pdf: $(DOCNAME).md tmp.bib $(DOCNAME).yml american-physics-society.csl Makefile
	pandoc $< --bibliography=$(word 2, $^) --defaults=$(word 3, $^) --csl=$(word 4, $^) -o $@

view: $(DOCNAME).pdf
	xdg-open $< &

clean:
	-rm *.pdf
