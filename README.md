# [statement-during-phd-md](https://gitlab.com/eidoom/statement-during-phd-md)

Tested with `pandoc` `2.11.2`.

Using [American Physical Society CSL](https://github.com/citation-style-language/styles/blob/master/american-physics-society.csl).

[Live here](https://eidoom.gitlab.io/statement-during-phd-md/statement.pdf)
