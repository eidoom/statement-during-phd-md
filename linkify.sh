#!/usr/bin/env bash

perl -0777p -e "
    1 while s|(title = \")([^\\\][^h][^r][^e][^f].*?)(\".*?url = \")(.*?)(\")|\1\\\href\{\4\}\{\2\}\3\4\5|gs; 
    " \
    $1 >$2
